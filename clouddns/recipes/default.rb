#
# Cookbook Name:: clouddns
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
 clouddns_managed_zone_managed_zone 'createdns' do
   project 'graphite-development'
   action :create_managed_zone
  #action :delete_managed_zone
  #action :list_managed_zone
  #action :get_managed_zone
   managed_zone_object ( {
   description: 'Create DNS Entry',
   dns_name: 'example0104.com'
   })
   end
