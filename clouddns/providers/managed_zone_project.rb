require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'managed_zone_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end

# Call the get method to the current resource if available
def load_current_resource
   begin
      puts "this is load_current_resource"
      @current_resource = get_instance_of(new_resource)
   rescue Exception => e
      Chef::Log.error("get instance of #{name} failed. Reason: #{e.message}")
      Chef::Log.error("get instance of #{name} failed. Reason: #{e.backtrace.inspect}")
   end

end

def self.client
   dns = Google::Apis::DnsV1
   client = dns::DnsService.new
   # Get the environment configured authorization
   scopes =  ['https://www.googleapis.com/auth/ndev.clouddns.readwrite']
   authorization = Google::Auth.get_application_default(scopes)
   client.authorization = authorization
   client
end

def client
   self.class.client
end

def make_object(type, name, params)
   klass = type.split('_').collect(&:capitalize).join
   p = params.symbolize_keys.fixnumify
   object = Object::const_get("Google::Apis::DnsV1::#{klass}").new(p)
   puts klass
   object
end

def build_params
   params = {
      project:new_resource.project

   }
   params.delete_if { |key, value| value.nil? }
   params
end


#
action :get_project do
   begin
      Chef::Log.info(" Calling API get_project")

      converge_by("get_project for  #{new_resource.project}") do
         begin
            response = get_project_impl(project)
         rescue Exception => e
            Chef::Log.error("API called failed with message: #{e.message}")
         end
      end
   end
end
def get_instance_of(new_resource)
   # Calling get API
   response = get_project_impl(new_resource.project)
   response
end

