#require_relative '../swagger/provider'
#require_relative '../swagger/fixnumify'
#require_relative '../swagger/symbolize_keys'
require_relative 'provider'
require_relative 'fixnumify'
require_relative 'symbolize_keys'
require 'googleauth'
require 'google/apis/dns_v1'

def self.error_msg(msg)
   puts msg
   exit false
end

def self.client
   api_class = Google::Apis::DnsV1
   client = api_class::DnsService.new
   # Get the environment configured authorization
   scopes =   ['https://www.googleapis.com/auth/ndev.clouddns.readwrite']
   authorization = Google::Auth.get_application_default(scopes)
   client.authorization = authorization
   client
end

def client
   self.class.client
end

def config
   self.class.config
end

def error_msg(msg)
   self.class.error_msg(msg)
end

def make_object(type, name, params)
   klass = type.split('_').collect(&:capitalize).join
   p = params.symbolize_keys.fixnumify
   object = Object::const_get("Google::Apis::DnsV1::#{klass}").new(p)
   object
end

######## Implementation of  Change Resource ############

#Override list Method of Change  Resources
def self.list_instances_of_with_resources_change(custom_params)
   project = custom_params[:project]
   managed_zone = custom_params[:managed_zone]
   max_results = (custom_params.has_key? :max_results) ? custom_params[:max_results] : nil
   page_token = (custom_params.has_key? :page_token) ? custom_params[:page_token] : nil
   sort_by = (custom_params.has_key? :sort_by) ? custom_params[:sort_by] : nil
   sort_order = (custom_params.has_key? :sort_order) ? custom_params[:sort_order] : nil
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   if managed_zone.nil? || managed_zone.empty?
      error_msg("The value for 'managed_zone' not defined in resource params or config file")
   end
   list_instances = client.send("list_changes", project, managed_zone, max_results:max_results, page_token:page_token, sort_by:sort_by, sort_order:sort_order)
   list_instances.instance_variables.each do |name|
      if list_instances.instance_variable_get(name).is_a? Array
         list_instances=list_instances.instance_variable_get(name)
      end
   end
   list_instances
end
def list_instances_of_change(id, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   managed_zone = optional_path_params[:managed_zone]
   max_results = (optional_query_params.has_key? :max_results) ? optional_query_params[:max_results] : nil
   page_token = (optional_query_params.has_key? :page_token) ? optional_query_params[:page_token] : nil
   sort_by = (optional_query_params.has_key? :sort_by) ? optional_query_params[:sort_by] : nil
   sort_order = (optional_query_params.has_key? :sort_order) ? optional_query_params[:sort_order] : nil
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   response = client.send("list_changes", project,id, max_results:max_results, page_token:page_token, sort_by:sort_by, sort_order:sort_order)
   response
end

#Override create Method of Change  Resources
def create_instances_of_change(id, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   managed_zone = optional_path_params[:managed_zone]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   if managed_zone.nil? || managed_zone.empty?
      error_msg("The value for 'managed_zone' not defined in resource params or config file")
   end
   response = client.send("create_change", project, managed_zone, make_object("change", name, build_params))
   response
end

#Override get Method of Change  Resources
def self.get_instances_of_with_resources_change(custom_params)
   project = custom_params[:project]
   managed_zone = custom_params[:managed_zone]
   change_id = custom_params[:change_id]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   if managed_zone.nil? || managed_zone.empty?
      error_msg("The value for 'managed_zone' not defined in resource params or config file")
   end
   if change_id.nil? || change_id.empty?
      error_msg("The value for 'change_id' not defined in resource params or config file")
   end
   get_instances = client.send("get_changes", project, managed_zone, change_id)
   get_instances
end
def get_instances_of_change(id, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   managed_zone = optional_path_params[:managed_zone]
   change_id = optional_path_params[:change_id]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   if managed_zone.nil? || managed_zone.empty?
      error_msg("The value for 'managed_zone' not defined in resource params or config file")
   end
   response = client.send("get_change", project, managed_zone,id)
   response
end

#Override update Method of Change  Resources
def update_instances_of_change(id, build_params, optional_path_params, optional_query_params)
   puts "No update operation available for this resource"
end
######## Implementation of  ManagedZone Resource ############

#Override list Method of ManagedZone  Resources
def self.list_instances_of_with_resources_managed_zone(custom_params)
   project = custom_params[:project]
   max_results = (custom_params.has_key? :max_results) ? custom_params[:max_results] : nil
   page_token = (custom_params.has_key? :page_token) ? custom_params[:page_token] : nil
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   list_instances = client.send("list_managed_zones", project, max_results:max_results, page_token:page_token)
   list_instances.instance_variables.each do |name|
      if list_instances.instance_variable_get(name).is_a? Array
         list_instances=list_instances.instance_variable_get(name)
      end
   end
   list_instances
end
def list_instances_of_managed_zone(name, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   max_results = (optional_query_params.has_key? :max_results) ? optional_query_params[:max_results] : nil
   page_token = (optional_query_params.has_key? :page_token) ? optional_query_params[:page_token] : nil
   response = client.send("list_managed_zones",project, max_results:max_results, page_token:page_token)
   response
end

#Override create Method of ManagedZone  Resources
#def create_instances_of_managed_zone(name, build_params,optional_path_params,optional_query_params)
def create_managed_zone_impl(name, managed_zone_object)
    puts "create_managed_zone_impl"
   project = optional_path_params[:project]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   puts "***************************--------"
   response = client.send("create_managed_zone", project,   managed_zone_object)
   response
end

#Override get Method of ManagedZone  Resources
def self.get_instances_of_with_resources_managed_zone(custom_params)
   project = custom_params[:project]
   managed_zone = custom_params[:managed_zone]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   if managed_zone.nil? || managed_zone.empty?
      error_msg("The value for 'managed_zone' not defined in resource params or config file")
   end
   get_instances = client.send("get_managed_zones", project, managed_zone)
   get_instances
end
def get_instances_of_managed_zone(name, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   managed_zone = optional_path_params[:managed_zone]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   response = client.send("get_managed_zone", project,name)
   response
end

#Override delete Method of ManagedZone  Resources
def delete_instances_of_managed_zone(name, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   managed_zone = optional_path_params[:managed_zone]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   response = client.send("delete_managed_zone", project,name)
   response
end

#Override update Method of ManagedZone  Resources
def update_instances_of_managed_zone(name, build_params, optional_path_params, optional_query_params)
   puts "No update operation available for this resource"
end
######## Implementation of  Project Resource ############

#Override get Method of Project  Resources
def self.get_instances_of_with_resources_project(custom_params)
   project = custom_params[:project]
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   get_instances = client.send("get_projects", project)
   get_instances
end
def get_instances_of_project(project, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   response = client.send("get_project",project)
   response
end

#Override update Method of Project  Resources
def update_instances_of_project(project, build_params, optional_path_params, optional_query_params)
   puts "No update operation available for this resource"
end
######## Implementation of  Quota Resource ############
######## Implementation of  ResourceRecordSet Resource ############

#Override list Method of ResourceRecordSet  Resources
def self.list_instances_of_with_resources_resource_record_set(custom_params)
   project = custom_params[:project]
   managed_zone = custom_params[:managed_zone]
   max_results = (custom_params.has_key? :max_results) ? custom_params[:max_results] : nil
   name = (custom_params.has_key? :name) ? custom_params[:name] : nil
   page_token = (custom_params.has_key? :page_token) ? custom_params[:page_token] : nil
   type = (custom_params.has_key? :type) ? custom_params[:type] : nil
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   if managed_zone.nil? || managed_zone.empty?
      error_msg("The value for 'managed_zone' not defined in resource params or config file")
   end
   list_instances = client.send("list_resource_record_sets", project, managed_zone, max_results:max_results, name:name, page_token:page_token, type:type)
   list_instances.instance_variables.each do |name|
      if list_instances.instance_variable_get(name).is_a? Array
         list_instances=list_instances.instance_variable_get(name)
      end
   end
   list_instances
end
def list_instances_of_resource_record_set(managed_zone, build_params,optional_path_params,optional_query_params)
   project = optional_path_params[:project]
   managed_zone = optional_path_params[:managed_zone]
   max_results = (optional_query_params.has_key? :max_results) ? optional_query_params[:max_results] : nil
   name = (optional_query_params.has_key? :name) ? optional_query_params[:name] : nil
   page_token = (optional_query_params.has_key? :page_token) ? optional_query_params[:page_token] : nil
   type = (optional_query_params.has_key? :type) ? optional_query_params[:type] : nil
   if project.nil? || project.empty?
      error_msg("The value for 'project' not defined in resource params or config file")
   end
   response = client.send("list_resource_record_sets", project,managed_zone, max_results:max_results, name:name, page_token:page_token, type:type)
   response
end

#Override update Method of ResourceRecordSet  Resources
def update_instances_of_resource_record_set(managed_zone, build_params, optional_path_params, optional_query_params)
   puts "No update operation available for this resource"
end

