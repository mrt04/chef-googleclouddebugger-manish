require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'managed_zone_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end

# Call the get method to the current resource if available
def load_current_resource
   begin
      puts "this is load_current_resource"
      @current_resource = get_instance_of(new_resource)
   rescue Exception => e
      Chef::Log.error("get instance of #{name} failed. Reason: #{e.message}")
      Chef::Log.error("get instance of #{name} failed. Reason: #{e.backtrace.inspect}")
   end

end

def self.client
   dns = Google::Apis::DnsV1
   client = dns::DnsService.new
   # Get the environment configured authorization
   scopes =  ['https://www.googleapis.com/auth/ndev.clouddns.readwrite']
   authorization = Google::Auth.get_application_default(scopes)
   client.authorization = authorization
   client
end

def client
   self.class.client
end

def make_object(type, name, params)
   klass = type.split('_').collect(&:capitalize).join
   p = params.symbolize_keys.fixnumify
   object = Object::const_get("Google::Apis::DnsV1::#{klass}").new(p)
   puts klass
   object
end

def build_params
   params = {
      change_id:new_resource.change_id,

      managed_zone:new_resource.managed_zone,

      project:new_resource.project,

      max_results:new_resource.max_results,

      page_token:new_resource.page_token,

      sort_by:new_resource.sort_by,

      sort_order:new_resource.sort_order,

      change_object:new_resource.change_object

   }
   params.delete_if { |key, value| value.nil? }
   params
end


#
action :get_change do
   begin
      Chef::Log.info(" Calling API get_change")

      converge_by("get_change for  #{new_resource.project} #{new_resource.managed_zone} #{new_resource.change_id}") do
         begin
            response = get_change_impl(project, managed_zone, change_id)
         rescue Exception => e
            Chef::Log.error("API called failed with message: #{e.message}")
         end
      end
   end
end
def get_instance_of(new_resource)
   # Calling get API
   response = get_change_impl(new_resource.project,new_resource. managed_zone,new_resource. change_id)
   response
end


#
action :list_changes do
   begin
      Chef::Log.info(" Calling API list_changes")

      converge_by("list_changes for  #{new_resource.project} #{new_resource.managed_zone}") do
         begin
            response = list_changes_impl(project, managed_zone, max_results, page_token, sort_by, sort_order)
         rescue Exception => e
            Chef::Log.error("API called failed with message: #{e.message}")
         end
      end
   end
end


#
action :create_change do
   begin
      Chef::Log.info(" Calling API create_change")
      if !(( new_resource.project == @current_resource.project ) && ( new_resource.managed_zone == @current_resource.managed_zone ))

         converge_by("create_change for  #{new_resource.project} #{new_resource.managed_zone}") do
            begin
               response = create_change_impl(project, managed_zone, change_object)
            rescue Exception => e
               Chef::Log.error("API called failed with message: #{e.message}")
            end
         end
      else
         Chef::Log.info("Resource state unchanged, not created / updated")
      end
   end
end

