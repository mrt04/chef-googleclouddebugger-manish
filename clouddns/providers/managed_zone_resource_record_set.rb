require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'managed_zone_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end

# Call the get method to the current resource if available
def load_current_resource
   begin
      puts "this is load_current_resource"
      @current_resource = get_instance_of(new_resource)
   rescue Exception => e
      Chef::Log.error("get instance of #{name} failed. Reason: #{e.message}")
      Chef::Log.error("get instance of #{name} failed. Reason: #{e.backtrace.inspect}")
   end

end

def self.client
   dns = Google::Apis::DnsV1
   client = dns::DnsService.new
   # Get the environment configured authorization
   scopes =  ['https://www.googleapis.com/auth/ndev.clouddns.readwrite']
   authorization = Google::Auth.get_application_default(scopes)
   client.authorization = authorization
   client
end

def client
   self.class.client
end

def make_object(type, name, params)
   klass = type.split('_').collect(&:capitalize).join
   p = params.symbolize_keys.fixnumify
   object = Object::const_get("Google::Apis::DnsV1::#{klass}").new(p)
   puts klass
   object
end

def build_params
   params = {
      managed_zone:new_resource.managed_zone,

      project:new_resource.project,

      max_results:new_resource.max_results,

      name:new_resource.name,

      page_token:new_resource.page_token,

      type:new_resource.type

   }
   params.delete_if { |key, value| value.nil? }
   params
end


#
action :list_resource_record_sets do
   begin
      Chef::Log.info(" Calling API list_resource_record_sets")

      converge_by("list_resource_record_sets for  #{new_resource.project} #{new_resource.managed_zone}") do
         begin
            response = list_resource_record_sets_impl(project, managed_zone, max_results, name, page_token, type)
         rescue Exception => e
            Chef::Log.error("API called failed with message: #{e.message}")
         end
      end
   end
end

