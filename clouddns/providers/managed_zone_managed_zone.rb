require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'managed_zone_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end

# Call the get method to the current resource if available
def load_current_resource
   begin
      puts "this is load_current_resource"
      @current_resource = get_instance_of(new_resource)
   rescue Exception => e
      Chef::Log.error("get instance of #{new_resource.name} failed. Reason: #{e.message}")
      Chef::Log.error("get instance of #{new_resource.name} failed. Reason: #{e.backtrace.inspect}")
   end

end

def self.client
   dns = Google::Apis::DnsV1
   client = dns::DnsService.new
   # Get the environment configured authorization
   scopes =  ['https://www.googleapis.com/auth/ndev.clouddns.readwrite']
   authorization = Google::Auth.get_application_default(scopes)
   client.authorization = authorization
   client
end

def client
   self.class.client
end

def make_object(type, name, params)
   klass = type.split('_').collect(&:capitalize).join
   p = params.symbolize_keys.fixnumify
   object = Object::const_get("Google::Apis::DnsV1::#{klass}").new(p)
   puts klass
   object
end

def build_params
   params = {
      project:new_resource.project,

      max_results:new_resource.max_results,

      page_token:new_resource.page_token,

      managed_zone_object:new_resource.managed_zone_object,

      managed_zone:new_resource.managed_zone


   }
   params.delete_if { |key, value| value.nil? }
   params
end

def optional_path_params
   params = {
      project: new_resource.project,
      #managed_zone: new_resource.managed_zone,
      }
            params.delete_if { |key, value| value.nil? }
               params
               end


#
action :list_managed_zones do
   begin
      Chef::Log.info(" Calling API list_managed_zones")

      converge_by("list_managed_zones for  #{new_resource.project}") do
         begin
            response = list_managed_zones_impl(project, max_results, page_token)
         rescue Exception => e
            Chef::Log.error("API called failed with message: #{e.message}")
         end
      end
   end
end


#
action :create_managed_zone do
   begin
      Chef::Log.info(" Calling API create_managed_zone")
      if @current_resource.nil? || !(( new_resource.project == @current_resource.project))

         converge_by("create_managed_zone for  #{new_resource.project}") do
            begin
               response = create_managed_zone_impl(new_resource.project, new_resource.managed_zone_object)
            rescue Exception => e
               Chef::Log.error("API called failed with message: #{e.message}")
            end
         end
      else
         Chef::Log.info("Resource state unchanged, not created / updated")
      end
   end
end


#
action :get_managed_zone do
   begin
      Chef::Log.info(" Calling API get_managed_zone")

      converge_by("get_managed_zone for  #{new_resource.project} #{new_resource.managed_zone}") do
         begin
            response = get_managed_zone_impl(project, managed_zone)
         rescue Exception => e
            Chef::Log.error("API called failed with message: #{e.message}")
         end
      end
   end
end
def get_instance_of(new_resource)
   # Calling get API
   response = get_managed_zone_impl(new_resource.project,new_resource. managed_zone)
   response
end


#
action :delete_managed_zone do
   begin
      Chef::Log.info(" Calling API delete_managed_zone")
      if ( new_resource.project == @current_resource.project ) && ( new_resource.managed_zone == @current_resource.managed_zone )

         converge_by("delete_managed_zone for  #{new_resource.project} #{new_resource.managed_zone}") do
            begin
               response = delete_managed_zone_impl(project, managed_zone)
            rescue Exception => e
               Chef::Log.error("API called failed with message: #{e.message}")
            end
         end
      else
         Chef::Log.info("Resource state unchanged, not deleted")
      end
   end
end

