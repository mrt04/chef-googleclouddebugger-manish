#Attributes
#get_project:path|Identifies the project addressed by this request.
attribute :project, :kind_of => String, :required => true


#Actions
actions :get_project #Fetch the representation of an existing Project.
