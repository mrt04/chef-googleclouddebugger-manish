#Attributes
#get_change:path|The identifier of the requested change, from a previous ResourceRecordSetsChangeResponse.
attribute :change_id, :kind_of => String, :required => true

#get_change:path|create_change:path|Identifies the managed zone addressed by this request. Can be the managed zone name or id.
attribute :managed_zone, :kind_of => String, :required => true

#get_change:path|create_change:path|Identifies the project addressed by this request.
attribute :project, :kind_of => String, :required => true

#list_changes:query|Optional. Maximum number of results to be returned. If unspecified, the server will decide how many results to return.
attribute :max_results, :kind_of => Integer

#list_changes:query|Optional. A tag returned by a previous list request that was truncated. Use this parameter to continue a previous list request.
attribute :page_token, :kind_of => String

#list_changes:query|Sorting criterion. The only supported value is change sequence. Acceptable values are: "changeSequence" (default)
attribute :sort_by, :kind_of => String

#list_changes:query|Sorting order direction: 'ascending' or 'descending'.
attribute :sort_order, :kind_of => String

#create_change:body|
attribute :change_object, :kind_of => Hash


#Actions
actions :get_change #Fetch the representation of an existing Change.,
:list_changes #Enumerate Changes to a ResourceRecordSet collection.,
:create_change #Atomically update the ResourceRecordSet collection.
