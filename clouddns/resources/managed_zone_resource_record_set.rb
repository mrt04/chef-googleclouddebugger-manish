#Attributes
#list_resource_record_sets:path|Identifies the managed zone addressed by this request. Can be the managed zone name or id.
attribute :managed_zone, :kind_of => String, :required => true

#list_resource_record_sets:path|Identifies the project addressed by this request.
attribute :project, :kind_of => String, :required => true

#list_resource_record_sets:query|Optional. Maximum number of results to be returned. If unspecified, the server will decide how many results to return.
attribute :max_results, :kind_of => Integer

#list_resource_record_sets:query|Restricts the list to return only records with this fully qualified domain name.
attribute :name, :kind_of => String

#list_resource_record_sets:query|Optional. A tag returned by a previous list request that was truncated. Use this parameter to continue a previous list request.
attribute :page_token, :kind_of => String

#list_resource_record_sets:query|Restricts the list to return only records of this type. If present, the "name" parameter must also be present.
attribute :type, :kind_of => String


#Actions
actions :list_resource_record_sets #Enumerate ResourceRecordSets that have been created but not yet deleted.
