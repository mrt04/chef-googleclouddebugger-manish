#Attributes
#register_controller_debuggee:body|
attribute :controller_debuggee_object, :kind_of => Hash


def controller_debuggee (arg = nil)
   if arg.nil? and @project.nil?
      @controller_debuggee_object[:title]
   else
      set_or_return( :project, arg, :kind_of => String )
   end
end

#Actions
actions :register_controller_debuggee
