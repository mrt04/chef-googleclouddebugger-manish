#Attributes
#list_debugger_debuggee_breakpoints:path|delete_debugger_debuggee_breakpoint:path|Identifies the debuggee being debugged.
attribute :debuggee_id, :kind_of => String, :required => true

#list_debugger_debuggee_breakpoints:query|When set to true, the response includes the list of breakpoints set by any user. Otherwise, it includes only breakpoints set by the caller.
attribute :include_all_users, :kind_of => [TrueClass, FalseClass]

#list_debugger_debuggee_breakpoints:query|When set to true, the response includes active and inactive breakpoints. Otherwise, it includes only active breakpoints.
attribute :include_inactive, :kind_of => [TrueClass, FalseClass]

#list_debugger_debuggee_breakpoints:query|When set, the response includes only breakpoints with the specified action.
attribute :action, :kind_of => String

#list_debugger_debuggee_breakpoints:query|When set to true, the response breakpoints are stripped of the results fields: stack_frames, evaluated_expressions and variable_table.
attribute :strip_results, :kind_of => [TrueClass, FalseClass]

#list_debugger_debuggee_breakpoints:query|A wait token that, if specified, blocks the call until the breakpoints list has changed, or a server selected timeout has expired. The value should be set from the last response. The error code google.rpc.Code.ABORTED (RPC) is returned on wait timeout, which should be called again with the same wait_token.
attribute :wait_token, :kind_of => String

#list_debugger_debuggee_breakpoints:query|delete_debugger_debuggee_breakpoint:query|The client version making the call. Following: domain/type/version (e.g., google.com/intellij/v1).
attribute :client_version, :kind_of => String

#set_debugger_debuggee_breakpoint:body|
attribute :debugger_debuggee_breakpoint_object, :kind_of => Hash

#get_debugger_debuggee_breakpoint:path|delete_debugger_debuggee_breakpoint:path|Breakpoint identifier, unique in the scope of the debuggee.
attribute :breakpoint_id, :kind_of => String, :required => true


def debugger_debuggee_breakpoint (arg = nil)
   if arg.nil? and @project.nil?
      @debugger_debuggee_breakpoint_object[:breakpoint_id]
   else
      set_or_return( :project, arg, :kind_of => String )
   end
end

#Actions
actions :list_debugger_debuggee_breakpoints,
:set_debugger_debuggee_breakpoint,
:get_debugger_debuggee_breakpoint,
:delete_debugger_debuggee_breakpoint
