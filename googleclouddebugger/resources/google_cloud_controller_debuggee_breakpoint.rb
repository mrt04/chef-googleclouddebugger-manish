#Attributes
#list_controller_debuggee_breakpoints:path|update_controller_debuggee_breakpoint:path|Identifies the debuggee.
attribute :debuggee_id, :kind_of => String, :required => true

#list_controller_debuggee_breakpoints:query|A wait token that, if specified, blocks the method call until the list of active breakpoints has changed, or a server selected timeout has expired. The value should be set from the last returned response.
attribute :wait_token, :kind_of => String

#list_controller_debuggee_breakpoints:query|if set to true, returns google.rpc.Code.OK status and sets the wait_expired response field to true when the server-selected timeout has expired (recommended).
attribute :success_on_timeout, :kind_of => [TrueClass, FalseClass]

#update_controller_debuggee_breakpoint:path|Breakpoint identifier, unique in the scope of the debuggee.
attribute :breakpoint_id, :kind_of => String, :required => true

#update_controller_debuggee_breakpoint:body|
attribute :controller_debuggee_breakpoint_object, :kind_of => Hash


def controller_debuggee_breakpoint (arg = nil)
   if arg.nil? and @project.nil?
      @controller_debuggee_breakpoint_object[:id]
   else
      set_or_return( :project, arg, :kind_of => String )
   end
end

#Actions
actions :list_controller_debuggee_breakpoints,
:update_controller_debuggee_breakpoint
