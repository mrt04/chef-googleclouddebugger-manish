#Attributes
#list_debugger_debuggees:query|Project number of a Google Cloud project whose debuggees to list.
attribute :project, :kind_of => String

#list_debugger_debuggees:query|When set to true, the result includes all debuggees. Otherwise, the result includes only debuggees that are active.
attribute :include_inactive, :kind_of => [TrueClass, FalseClass]


#list_debugger_debuggees:query|The client version making the call. Following: domain/type/version (e.g., google.com/intellij/v1).
attribute :client_version, :kind_of => String


def debugger_debuggee (arg = nil)
   if arg.nil? and @project.nil?
      @debugger_debuggee_object[:title]
   else
      set_or_return( :project, arg, :kind_of => String )
   end
end

#Actions
actions :list_debugger_debuggees
