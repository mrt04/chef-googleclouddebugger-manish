 #  cookbook Name:: googleclouddebugger
 # Recipe:: controller_debugee_breakpoint
 #
 # Copyright 2016, YOUR_COMPANY_NAME
 #
 # All rights reserved - Do Not Redistribute
 #
 googleclouddebugger_google_cloud_controller_debuggee_breakpoint 'listbreakpoints' do
   debuggee_id 'gcp:994996842918:f4dcaa07c142b128'
 # wait_token
 # success_on_timeout
   action :list_controller_debuggee_breakpoints
 # action :update_controller_debuggee_breakpoint
 end

 googleclouddebugger_google_cloud_controller_debuggee_breakpoint 'listbreakpoints' do
   debuggee_id 'gcp:994996842918:f4dcaa07c142b128'
   breakpoint_id '537fc4b73c012-9df1-18815'
   controller_debuggee_breakpoint_object ({
       debuggee_id: 'gcp:994996842918:f4dcaa07c142b128',
       breakpoint_id: '537fc4b73c012-9df1-18815'
                       })
   action :update_controller_debuggee_breakpoint
 end

