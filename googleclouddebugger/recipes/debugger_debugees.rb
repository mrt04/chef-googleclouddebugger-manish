
 #  cookbook Name:: googleclouddebugger
 # Recipe:: debugger__debugees
 #
 # Copyright 2016, YOUR_COMPANY_NAME
 #
 # All rights reserved - Do Not Redistribute
 #
 googleclouddebugger_google_cloud_debugger_debuggee 'listdebugees' do
   project '994996842918'
   include_inactive true
   action :list_debugger_debuggees
 end
