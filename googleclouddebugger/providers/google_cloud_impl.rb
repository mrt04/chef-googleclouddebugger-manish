require 'googleauth'
require 'json'
require 'google/apis/clouddebugger_v2'

$LOAD_PATH.unshift ::File.expand_path("../../", __FILE__)

require 'libraries/fixnumify'
require 'libraries/symbolize_keys'


#def self.error_msg(msg)
def error_msg(msg)
   puts msg
   exit false
end

#def self.client
def client

   api_class = Google::Apis::ClouddebuggerV2
   client = api_class::CloudDebuggerService .new
   # Get the environment configured authorization
   scopes =   ['https://www.googleapis.com/auth/cloud-platform','https://www.googleapis.com/auth/cloud_debugger','https://www.googleapis.com/auth/cloud_debugletcontroller']
   authorization = Google::Auth.get_application_default(scopes)
   client.authorization = authorization
   client
end
=begin
def client
   self.class.client
end

def config
   self.class.config
end

def error_msg(msg)
   self.class.error_msg(msg)
end
=end

def make_object(klass, params)
   klass = klass.split('_').collect(&:capitalize).join
   p = params.symbolize_keys.fixnumify
#   controller_debuggee_object = Object::const_get("Google::Apis::CloudDebuggerV2::#{klass}").new(p)
   controller_debuggee_object = Object::const_get("Google::Apis::ClouddebuggerV2::#{klass}").new(p)
   controller_debuggee_object
end

#
#  Implementation for ControllerDebuggee
#
def register_controller_debuggee_impl(controller_debuggee_object)
   # Form object if required
   controller_debuggee_object = make_object("ControllerDebuggee", controller_debuggee_object)
#   response = client.send("register_controller_debuggee",controller_debuggee_object, controller_debuggee_object)
#modified after generation
   response = client.send("register_debuggee", make_object("register_debuggee_request", build_params))
   response
end


#
#  Implementation for ControllerDebuggeeBreakpoint
#
def list_controller_debuggee_breakpoints_impl(debuggee_id, wait_token, success_on_timeout)
   response = client.send("list_controller_debuggee_breakpoints",debuggee_id,wait_token: wait_token,success_on_timeout: success_on_timeout)
   response
end
def update_controller_debuggee_breakpoint_impl(debuggee_id, breakpoint_id, controller_debuggee_breakpoint_object)
   # Form object if required
   #controller_debuggee_breakpoint_object = make_object("ControllerDebuggeeBreakpoint", controller_debuggee_breakpoint_object)
#   response = client.send("update_controller_debuggee_breakpoint",debuggee_id,breakpoint_id, controller_debuggee_breakpoint_object)
#modified after generation
   #response = client.send("update_active_breakpoint", debuggee_id,breakpoint_id, make_object("update_active_breakpoint_request", build_params))
   response = client.send("update_active_breakpoint", debuggee_id,breakpoint_id, make_object("update_active_breakpoint_request", controller_debuggee_breakpoint_object))
   response
end


#
#  Implementation for DebuggerDebuggee
#
def list_debugger_debuggees_impl(project, include_inactive, client_version)
   response = client.send("list_debugger_debuggees",project: project,include_inactive: include_inactive,client_version: client_version)
   response
end


#
#  Implementation for DebuggerDebuggeeBreakpoint
#
def list_debugger_debuggee_breakpoints_impl(debuggee_id, include_all_users, include_inactive, action, strip_results, wait_token, client_version)
   response = client.send("list_debugger_debuggee_breakpoints",debuggee_id,include_all_users: include_all_users,include_inactive: include_inactive,action_value: action,strip_results: strip_results,wait_token: wait_token,client_version: client_version)
   response
end
def set_debugger_debuggee_breakpoint_impl(debuggee_id, client_version, debugger_debuggee_breakpoint_object)
   # Form object if required
   debugger_debuggee_breakpoint_object = make_object("DebuggerDebuggeeBreakpoint", debugger_debuggee_breakpoint_object)
#   response = client.send("set_debugger_debuggee_breakpoint",debuggee_id,client_version, debugger_debuggee_breakpoint_object)
#modified after generation
   response = client.send("set_debugger_debuggee_breakpoint",debuggee_id, make_object("breakpoint", build_params),client_version:client_version)
   response
end
def get_debugger_debuggee_breakpoint_impl(debuggee_id, breakpoint_id, client_version)
   response = client.send("get_debugger_debuggee_breakpoint",debuggee_id,breakpoint_id,client_version: client_version)
   response
end
def delete_debugger_debuggee_breakpoint_impl(debuggee_id, breakpoint_id, client_version)
   response = client.send("delete_debugger_debuggee_breakpoint",debuggee_id,breakpoint_id,client_version: client_version)
   response
end

