require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'google_cloud_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end


action :list_debugger_debuggee_breakpoints do
   begin
      Chef::Log.info(" Calling API list_debugger_debuggee_breakpoints")
      response = list_debugger_debuggee_breakpoints_impl(new_resource.debuggee_id, new_resource.include_all_users, new_resource.include_inactive, new_resource.action, new_resource.strip_results, new_resource.wait_token, new_resource.client_version)
   end
end


action :set_debugger_debuggee_breakpoint do
   begin
      if @current_resource.nil? || !(( new_resource.debuggee_id[:breakpoint_id] == @current_resource.breakpoint_id ) && ( new_resource.debugger_debuggee_breakpoint_object[:breakpoint_id] == @current_resource.breakpoint_id ))
         converge_by("set_debugger_debuggee_breakpoint for  #{new_resource.debuggee_id}") do
            Chef::Log.info(" Calling API set_debugger_debuggee_breakpoint")
            response = set_debugger_debuggee_breakpoint_impl(new_resource.debuggee_id, new_resource.client_version, new_resource.debugger_debuggee_breakpoint_object)
         end
      else
         Chef::Log.info("Resource state unchanged, not created / updated")
      end
   end
end


action :get_debugger_debuggee_breakpoint do
   begin
      Chef::Log.info(" Calling API get_debugger_debuggee_breakpoint")
      response = get_debugger_debuggee_breakpoint_impl(new_resource.debuggee_id, new_resource.breakpoint_id, new_resource.client_version)
   end
end
def get_instance_of(new_resource)
   # Calling get API
   response = get_debugger_debuggee_breakpoint_impl(new_resource.debuggee_id, new_resource.breakpoint_id, new_resource.client_version)
   response
end


action :delete_debugger_debuggee_breakpoint do
   begin
      if !@current_resource.nil? and new_resource.debuggee_id == @current_resource.breakpoint_id && new_resource.breakpoint_id == @current_resource.breakpoint_id
         converge_by("delete_debugger_debuggee_breakpoint for  #{new_resource.debuggee_id} #{new_resource.breakpoint_id}") do
            Chef::Log.info(" Calling API delete_debugger_debuggee_breakpoint")
            response = delete_debugger_debuggee_breakpoint_impl(new_resource.debuggee_id, new_resource.breakpoint_id, new_resource.client_version)
         end
      else
         Chef::Log.info("Resource state unchanged, not deleted")
      end
   end
end


# Call the get method to the current resource if available
def load_current_resource
   begin
      puts "this is load_current_resource"
      @current_resource = get_instance_of(new_resource)
      hash = @current_resource.instance_variables.each_with_object({}) { |var, hash| hash[var.to_s.delete("@")] = @current_resource.instance_variable_get(var) }
      puts "Current Resource:: #{hash.to_s}"
   rescue Exception => e
      Chef::Log.error("get instance of debugger_debuggee_breakpoint failed. Reason: #{e.message}")
      Chef::Log.error("get instance of debugger_debuggee_breakpoint failed. Reason: #{e.backtrace.inspect}")
   end
end
