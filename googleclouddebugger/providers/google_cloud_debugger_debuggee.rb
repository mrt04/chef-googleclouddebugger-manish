require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'google_cloud_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end


action :list_debugger_debuggees do
   begin
      Chef::Log.info(" Calling API list_debugger_debuggees")
      response = list_debugger_debuggees_impl(new_resource.project, new_resource.include_inactive, new_resource.client_version)
   end
end


