require 'google/apis/clouddebugger_v2'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'google_cloud_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end


action :list_controller_debuggee_breakpoints do
   begin
      Chef::Log.info(" Calling API list_controller_debuggee_breakpoints")
      response = list_controller_debuggee_breakpoints_impl(new_resource.debuggee_id, new_resource.wait_token, new_resource.success_on_timeout)
   end
end


action :update_controller_debuggee_breakpoint do
   begin
      Chef::Log.info(" Calling API update_controller_debuggee_breakpoint")
      response = update_controller_debuggee_breakpoint_impl(new_resource.debuggee_id, new_resource.breakpoint_id, new_resource.controller_debuggee_breakpoint_object)
   end
end


