require 'google/apis/dns_v1'
require 'googleauth'
require 'json'

$LOAD_PATH.unshift ::File.expand_path("../", __FILE__)
require 'google_cloud_impl.rb'

# Support whyrun
def whyrun_supported?
   true
end


action :register_controller_debuggee do
   begin
      if @current_resource.nil? || !(( new_resource.controller_debuggee_object[:title] == @current_resource.title ))
         converge_by("register_controller_debuggee for ") do
            Chef::Log.info(" Calling API register_controller_debuggee")
            response = register_controller_debuggee_impl(new_resource.controller_debuggee_object)
         end
      else
         Chef::Log.info("Resource state unchanged, not created / updated")
      end
   end
end


